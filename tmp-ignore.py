import ioutils
from version import Version

from prettytable import PrettyTable 

import pathlib
import requests
import json
import tempfile
import os

INPUT = [
    ["base-files", "qemux86_64", "3.0.14"],
    ["base-passwd", "core2_64", "3.5.29"],
    ["busybox", "core2_64", "1.31.1"],
    ["busybox-hwclock", "core2_64", "1.31.1"],
    ["busybox-syslog", "core2_64", "1.31.1"],
    ["busybox-udhcpc", "core2_64", "1.31.1"],
    ["eudev", "core2_64", "3.2.9"],
    ["init-ifupdown", "qemux86_64", "1.0"],
    ["initscripts", "core2_64", "1.0"],
    ["initscripts-functions", "core2_64", "1.0"],
    ["kernel-5.4.69-yocto-standard", "qemux86_64", "5.4.69+git0+7f765dcb29_cfcdd63145"],
    ["kernel-module-uvesafb-5.4.69-yocto-standard", "qemux86_64", "5.4.69+git0+7f765dcb29_cfcdd63145"],
    ["ldconfig", "core2_64", "2.31+git0+6fdf971c9d"],
    ["libc6", "core2_64", "2.31+git0+6fdf971c9d"],
    ["libblkid1", "core2_64", "2.35.1"],
    ["libkmod2", "core2_64", "26"],
    ["libz1", "core2_64", "1.2.11"],
    ["modutils-initscripts", "core2_64", "1.0"],
    ["netbase", "core2_64", "6.1"],
    ["packagegroup-core-boot", "qemux86_64", "1.0"],
    ["run-postinsts", "noarch", "1.0"],
    ["sysvinit", "core2_64", "2.96"],
    ["sysvinit-inittab", "qemux86_64", "2.88dsf"],
    ["sysvinit-pidof", "core2_64", "2.96"],
    ["update-alternatives-opkg", "core2_64", "0.4.2"],
    ["update-rc.d", "noarch", "0.8"],
    ["v86d", "qemux86_64", "0.1.10"]
]

# FROM ~/ohos/build-ohos-linux-qemux86-64$ ../sources/poky/scripts/oe-pkgdata-util package-info -f ../build-ohos-linux-qemux86-64/tmp/deploy/images/qemux86-64/core-image-minimal-qemux86-64.manifest | grep -v "RPROVIDES" | cut -d" " -f 3,4 | sort | uniq
INPUT = [
    ["base-files", "XXX", "3.0.14-r89"],
    ["base-passwd", "XXX", "3.5.29-r0"],
    ["busybox", "XXX", "1.31.1-r0"],
    ["eudev", "XXX", "3.2.9-r0"],
    ["glibc", "XXX", "2.31+gitAUTOINC+6fdf971c9d-r0"],
    ["init-ifupdown", "XXX", "1.0-r7"],
    ["initscripts", "XXX", "1.0-r155"],
    ["kmod", "XXX", "26-r0"],
    ["linux-yocto", "XXX", "5.4.69+gitAUTOINC+7f765dcb29_cfcdd63145-r0"],
    ["modutils-initscripts", "XXX", "1.0-r7"],
    ["netbase", "XXX", "2:6.1-r0"],
    ["opkg-utils", "XXX", "0.4.2-r0"],
    ["packagegroup-core-boot", "XXX", "1.0-r17"],
    ["run-postinsts", "XXX", "1.0-r10"],
    ["sysvinit", "XXX", "2.96-r0"],
    ["sysvinit-inittab", "XXX", "2.88dsf-r10"],
    ["update-rc.d", "XXX", "0.8-r0"],
    ["util-linux", "XXX", "2.35.1-r0"],
    ["v86d", "XXX", "0.1.10-r2"],
    ["zlib", "XXX", "1.2.11-r0"]
]

# INPUT = [
#     ["busybox", "core2_64", "1.31.1"]
# ]

INPUT = [
    ["zlib", "XXX", "1.2.11-r0"]
]

tbl = PrettyTable()

tbl.field_names = [
    "yocto_package",
    "yocto_version",
    "yocto_arch",
    "debian_package",
    "debian_version",
    "debian_distribution",
    "version_distance",
    "message"
]

def main():
    print("Searching similar Yocto packages on debian repositories...")
    yocto_manifest = INPUT#[5:14]

    count = len(yocto_manifest)
    for i, v in enumerate(yocto_manifest):
        package = v[0]
        arch = v[1]
        resp = search(package, v[2])

        version = Version(v[2])
        print(f"[{i+1}/{count}] --> {package}; {version}; {arch} --> ", end='')
        if isinstance(resp, str):
            tbl.add_row([package, version, arch, "", "", "", Version.MAX_DISTANCE, resp])
        else:
            tbl.add_row([package, version, arch, resp[0], resp[1], resp[2], resp[3], resp[4]])
        
        print("Ready.")

    tbl.sortby = "version_distance"
    # tbl.reversesort = True
    print(tbl.get_string())
    
def search(yocto_package, version_str):

    version = Version(version_str)

    if version.has_flag(Version.FLAG_DEB_VERSION_ERROR):
        return "NOT A DEBIAN VERSION"

    yocto_input = [version, yocto_package, True, [], 0]
    l = []
    l.append(yocto_input)

    #print(f"# Package has a correct debian version {yocto_package}:")

    API_URL_JSON = "https://api.ftp-master.debian.org/madison?f&package="
    # API_URL_TEXT = "https://api.ftp-master.debian.org/madison?package="

    try:
        currpath = pathlib.Path(__file__).parent.absolute()
        f = open(f"{currpath}/tmp/api-resp-" + yocto_package)
        response = f.read()
    except FileNotFoundError:
        response = requests.get(API_URL_JSON + yocto_package)
        f = open(f"{currpath}/tmp/api-resp-" + yocto_package, "w")
        f.write(response.text)
        response = response.text
    finally:
        if f:
            f.close()

    json_response = json.loads(response)

    if not json_response: 
        return "NOT FOUND"

    j = json_response[0]
    for revision in j[yocto_package]:
        for vers_str in j[yocto_package][revision]:
            version = Version(vers_str)
            ver_distance = version.distance(yocto_input[0])
            l.append([version, yocto_package, False, [revision], ver_distance])

    ll = sorted(l, reverse=True)

    #print("# Searching these packages:")
    last_entry = ll[0]
    cleaned_ll = [last_entry]
    cleaned_i = 0
    for i in range(0, len(ll)):
        if last_entry[0] == ll[i][0]:
            cleaned_ll[cleaned_i][3].append(ll[i][3][0])
        else:
            cleaned_ll.append(ll[i])
            cleaned_i += 1
            last_entry = ll[i]
        #print(f"{i:0>2} - {ll[i]}")

    i = 0
    for v in cleaned_ll:
        if v[2] == True:
            break
        i += 1

    # find 2-nearest neighbors and take the one with the smallest distance
    nn1 = cleaned_ll[i-1] if i > 0 else [None,None,None,None,Version.MAX_DISTANCE]
    nn2 = cleaned_ll[i+1] if i + 1 < len(cleaned_ll) else [None,None,None,None,Version.MAX_DISTANCE]

    v = nn1 if nn1[4] < nn2[4] else nn2

    ver_distance = v[0].distance(yocto_input[0])
    return (
        v[1],
        v[0],
        v[3],
        ver_distance,
        version.distance_note(ver_distance)
    )
    
def clean_name(fn, to_clean):
    result = []
    spl = fn.split(os.path.sep)
    for el in spl:
        if el == '.' or el == to_clean:
            continue
        result.append(el)

    return os.path.sep.join(result)

def io_create_temp():
    temp_path = tempfile.mkdtemp(prefix='y2alien-')
    os.mkdir(f'{temp_path}/yocto')
    os.mkdir(f'{temp_path}/debian')
    return temp_path

def io_extract_archive(archive_path, dest):
    if archive_path.endswith('.gz'):
        tar_param = 'z'
    elif archive_path.endswith('.xz'):
        tar_param = 'J'
    else:
        print("ERROR: Archive type unknown.")
    stdout, stderr = ioutils.bash(
        f'tar {tar_param}xvf {archive_path} -C {dest}'
    )
    # print("stdout = " + stdout)
    # print("stderr = " + stderr)

def io_folder_diff(f1, f2):
    stdout, stderr = ioutils.bash(
        f'diff -qriwBbd {f1} {f2} || true'
    )
    return stdout.split('\n')

if __name__ == '__main__':
    main()
    print("Ready.")