import subprocess
import sys
import os
import requests

class IOUtilsError(Exception):
    pass

def bash(command, cwd=None, exception=Exception):
    """Run a command in bash shell, and return stdout and stderr
    :param command: the command to run
    :param cwd: directory where to run the command (defaults to current dir)
    :param exception: class to use to raise exceptions (defaults to 'Exception')
    :type command: str
    :type cwd: str
    :type exception: Exception subclass
    :return: stdout and stderr of the command
    :rtype: str list
    :raises: exception (see above) if command exit code != 0
    """
    out = subprocess.run(
        command, shell=True, executable="/bin/bash", cwd=cwd,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE,
    )
    stdout = out.stdout.decode()
    stderr = out.stderr.decode()
    if out.returncode != 0:
        raise exception(
            f'Command \'{command}\' failed. Output is:\n'
            f'{stdout}\n{stderr}'
        )
    return stdout, stderr

def io_file_checksum(file_path):
    stdout, stderr = bash(
        f'sha1sum {file_path}'
    )
    return stdout.split(' ', 1)[0]

def archive_readfile(archive_path, file_path):
    stdout, stderr = bash(
        f'tar {__get_tar_param(archive_path)}xvf {archive_path} {file_path} --to-command=cat'
    )
    result = stdout.split('\n')
    return result[0], result[1:]

def archive_checksums(archive_path, file_path):
    stdout, stderr = bash(
        f'tar {__get_tar_param(archive_path)}xvf {archive_path} {file_path} --to-command=sha1sum'
    )
    return __parse_sha1sum(stdout)

def archive_in_archive_checksums(archive_path, archive_in_archive, file_path=""):
    stdout, stderr = bash(
        f'tar {__get_tar_param(archive_path)}xvf {archive_path} {archive_in_archive} --to-command="tar {__get_tar_param(archive_in_archive)}xvf - {file_path} --to-command=sha1sum"'
    )
    return __parse_sha1sum(stdout)

def __get_tar_param(archive):
    if archive.endswith('.gz'):
        return 'z'
    if archive.endswith('.xz'):
        return 'J'
    raise IOUtilsError(f"Archive type unknown for {archive}.")

def __parse_sha1sum(sha1stdout):
    lines = sha1stdout.split('\n')
    files = {}
    i = 0
    while i < len(lines):
        if lines[i].endswith('/') or not __check_next_endswith(lines, i+1, '-') or not lines[i]:
            i += 1
            continue
        path = '/'.join(lines[i].split('/')[1:])
        files[path] = lines[i+1].split(' ', 1)[0]
        i += 2
    return files

def __check_next_endswith(lines, index, ends):
    if index >= len(lines) or index < 0:
        return False

    return lines[index].endswith(ends)
