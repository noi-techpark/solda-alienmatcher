#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2021 Peter Moser <p.moser@noi.bz.it>
#
# Class to match an entry inside a yocto manifest file with debian packages
# libraries through an API, exactly or if not possible find the closest smaller
# version.
#
# Information gathering:
# - Semantic versioning: https://semver.org/
# - Debian versioning: https://readme.phys.ethz.ch/documentation/debian_version_numbers/
#
# APIs used (by example):
# - https://api.ftp-master.debian.org/madison?S&package=busybox
# - https://launchpad.net/debian/+source/base-passwd/3.5.29

from version import Version
from alienpackage import Package, AlienPackage
import ioutils

from enum import Enum
import requests
import json
import collections as col
import os
import sys
from debian.deb822 import Deb822
from typing import Union

from spdx.file import File as SPDXFile
from spdx.checksum import Algorithm as SPDXAlgorithm
from spdx.parsers.tagvalue import Parser as SPDXTagValueParser
from spdx.parsers.tagvaluebuilders import Builder as SPDXTagValueBuilder
from spdx.parsers.loggers import StandardLogger as SPDXWriterLogger
from spdx.writers.tagvalue import write_document
from spdx.document import License as SPDXLicense
from spdx import utils

from debian2spdx import Debian2SPDX

VERSION = "0.1"

class AlienMatcherError(Exception):
    pass

class AlienMatcher:

    POOL_RELPATH_TMP = "apiresponse"
    POOL_RELPATH_DEBIAN = "debian"
    POOL_RELPATH_USERLAND = "userland"
    POOL_DEBIAN_BASEURL = "http://deb.debian.org/debian/pool/main"
    API_URL_JSON = "https://api.ftp-master.debian.org/madison?f&package="

    def __init__(self, path_to_pool):
        print(f"# Initializing alien-matcher with cache pool at {path_to_pool}.")
        super().__init__()
        path_to_pool = os.path.abspath(path_to_pool)
        self.pool_path = path_to_pool
        self.mkdir()
        if not os.path.isdir(path_to_pool):
            raise NotADirectoryError(
                f"Unable to create the POOL at path '{path_to_pool}'."
            )
        self.debian_path = self.mkdir(self.POOL_RELPATH_DEBIAN)
        self.userland_path = self.mkdir(self.POOL_RELPATH_USERLAND)
        self.tmp_path = self.mkdir(self.POOL_RELPATH_TMP)

        print(f"| Pool and directory structure created:")
        print(f"|   - Debian Path          : {self.debian_path}")
        print(f"|   - Userland Path        : {self.userland_path}")
        print(f"|   - Temporary Files Path : {self.tmp_path}")
        print(f"+-- SUCCESS.")

    def add_to_userland(self, alienpackage: AlienPackage):
        if not isinstance(alienpackage, AlienPackage):
            raise TypeError("Parameter must be a AlienPackage.")
        self.__add(
            self.POOL_RELPATH_USERLAND,
            alienpackage.name,
            alienpackage.version.str,
            alienpackage.archive_fullpath
        )

    def add_to_debian(self, package: Package):
        if not isinstance(package, Package):
            raise TypeError("Parameter must be a Package.")
        self.__add(
            self.POOL_RELPATH_DEBIAN,
            package.name,
            package.version,
            package.archive_fullpath
        )

    def mkdir(self, sub_folder = ""):
        path = self.__subpath(sub_folder)
        os.makedirs(
            path,
            mode = 0o755,
            exist_ok = True
        )
        return path

    def search(self, package: Package):
        print(f"# Search for similar packages with {self.API_URL_JSON}.")
        if not isinstance(package, Package):
            raise TypeError("Parameter must be a Package.")

        if package.version.has_flag(Version.FLAG_DEB_VERSION_ERROR):
            raise AlienMatcherError(
                f"'{package.name}' has no parseable debian version: {package.version.str}."
            )
        print(f"| Package version {package.version.str} has a valid Debian versioning format.")
        candidate_list = [
            [package.version, 0, True]
        ]

        api_response_cached = self.__subpath(
            self.POOL_RELPATH_TMP,
            f"api-resp-{package.name}.json"
        )
        print(f"| Search cache pool for existing API response.")
        try:
            response = self.get(api_response_cached)
            print(f"| API call result found in cache at {api_response_cached}.")
        except FileNotFoundError:
            print(f"| API call result not found in cache. Making an API call...")
            response = requests.get(self.API_URL_JSON + package.name)
            with open(api_response_cached, "w") as f:
                f.write(response.text)
            response = response.text

        json_response = json.loads(response)

        if not json_response:
            print(f"| No API response for package name {package.name}.")
            print(f"+-- FAILURE.")
            return None

        print(f"| API call result OK. Find nearest neighbor of {package.name}/{package.version.str}.")

        seen = set()
        j = json_response[0]
        for revision in j[package.name]:
            for vers_str in j[package.name][revision]:
                if vers_str in seen:
                    continue
                version = Version(vers_str)
                ver_distance = version.distance(package.version)
                candidate_list.append([version, ver_distance, False])
                seen.add(vers_str)


        candidate_list = sorted(candidate_list, reverse=True)

        i = 0
        for v in candidate_list:
            if v[2] == True:
                break
            i += 1

        # find 2-nearest neighbors and take the one with the smallest distance
        try:
            nn1 = candidate_list[i-1]
        except IndexError:
            nn1 = [None, Version.MAX_DISTANCE]
        try:
            nn2 = candidate_list[i+1]
        except IndexError:
            nn2 = [None, Version.MAX_DISTANCE]

        best_version = nn1[0] if nn1[1] < nn2[1] else nn2[0]

        print(f"| Nearest neighbor on Debian is {package.name}/{best_version.str}.")
        print(f"+-- SUCCESS.")

        return Package(name = package.name, version = best_version)

    def __subpath(self, *sub_folders):
        if sub_folders:
            return os.path.join(self.pool_path, *sub_folders)
        return self.pool_path

    def __add(self, pool_relpath, package_name, package_version, archive_fullpath):
        path = self.__subpath(
            pool_relpath,
            package_name,
            package_version
        )
        self.mkdir(path)
        archive_filename = os.path.basename(archive_fullpath)
        self.__copy(
            archive_fullpath,
            os.path.join(path, archive_filename)
        )
        print(f"| Adding package '{package_name}/{package_version}' to '{pool_relpath}'.")

    def get(self, *path_args):
        return self.__get(False, *path_args)

    def get_binary(self, *path_args):
        return self.__get(True, *path_args)

    def __get(self, binary, *path_args):
        path = self.__subpath(*path_args)
        flag = "b" if binary else ""
        with open(path, f'r{flag}') as f:
            return f.read()

    def __copy(self, src_filename, dst_filename):
        with open(src_filename, 'rb') as fr:
            with open(dst_filename, 'wb') as fw:
                fw.write(fr.read())

    def download_to_debian(self, package_name, package_version, filename):
        print(f"# Retrieving file from Debian: '{package_name}/{package_version}/{filename}'.")
        try:
            response = self.get_binary(
                self.POOL_RELPATH_DEBIAN,
                package_name,
                package_version,
                filename
            )
            print(f"| Found in Debian cache pool.")
        except FileNotFoundError:
            full_url = "/".join([
                self.POOL_DEBIAN_BASEURL,
                package_name[0],
                package_name,
                filename
            ])
            print(f"| Not found in Debian cache pool. Downloading from {full_url}.")
            r = requests.get(full_url)
            local_path = self.__subpath(
                self.POOL_RELPATH_DEBIAN,
                package_name,
                package_version
            )
            self.mkdir(local_path)
            with open(os.path.join(local_path, filename), 'wb+') as f:
                f.write(r.content)
            print(f"| Result cached in {os.path.join(local_path, filename)}.")
            response = r.content
        return response

    def fetch_debian_sources(self, package: Package):
        dsc_filename = f'{package.name}_{package.version.str}.dsc'
        dsc_file_content = self.download_to_debian(
            package.name,
            package.version.str,
            dsc_filename
        )

        debsrc_orig = ""
        debsrc_debian = ""
        debian_control = Deb822(dsc_file_content)

        if not debian_control['Format'].startswith('3.0'):
            raise AlienMatcherError(
                f"ERROR: We support only Debian Source Control files of format 3.0 at the moment: {debian_control['Format']} given."
            )

        debian_control_files = []
        for line in debian_control['Checksums-Sha1'].split('\n'):
            elem = line.strip().split()
            if len(elem) != 3:
                continue
            debian_control_files.append(elem)
            self.download_to_debian(package.name, package.version.str, elem[2])
            debian_path = self.__subpath(
                self.POOL_RELPATH_DEBIAN,
                package.name,
                package.version.str,
                elem[2]
            )
            chksum = ioutils.io_file_checksum(debian_path)
            if chksum != elem[0]:
                raise AlienMatcherError("ERROR: Checksum mismatch")
            if 'debian' in elem[2]:
                debsrc_debian = debian_path
            elif 'orig' in elem[2]:
                debsrc_orig = debian_path

        return debsrc_debian, debsrc_orig

    def __create_spdxfile(self, filename, checksum, license_info, copyright_text):
        spdxfile = SPDXFile(
            filename,
            chk_sum = SPDXAlgorithm(
                'SHA1',
                checksum
            )
        )
        spdxfile.licenses_in_file = [
            license_info
        ]
        spdxfile.conc_lics = license_info
        spdxfile.copyright = copyright_text
        return spdxfile

    def match(self, apkg: AlienPackage):
        print("# Find a matching package on Debian repositories.")

        spdx_new_filepath = self.__subpath(
            self.POOL_RELPATH_USERLAND,
            apkg.name,
            apkg.version.str,
            f'{apkg.name}_{apkg.version.str}.spdx'
        )

        # First, look if result is already cached
        print("| Search the cache pool.")
        try:
            result_spdx = self.get(spdx_new_filepath)
            print(f"| Found in cache at {spdx_new_filepath}.")
            print(f"+-- SUCCESS.")
            return result_spdx
        except FileNotFoundError:
            pass        # OK, not found, get to work...

        self.add_to_userland(apkg)
        match = self.search(apkg)

        if not match:
            return None

        spdx_orig_filepath = self.__subpath(
            self.POOL_RELPATH_DEBIAN,
            match.name,
            match.version.str,
            f"{apkg.name}_{match.version.str}.spdx"
        )

        print(f"| Generating SPDX file for Debian package at {spdx_orig_filepath}.")
        try:
            file_content = self.get(spdx_orig_filepath)
        except FileNotFoundError:
            debsrc_debian, debsrc_orig = self.fetch_debian_sources(match)
            d2s = Debian2SPDX(debsrc_orig, debsrc_debian)
            d2s.generate_SPDX()
            d2s.write_SPDX(spdx_orig_filepath)
            file_content = self.get(spdx_orig_filepath)

        print(f"| Testing original Debian SPDX file: (no output means no problems)")
        spdxparser = SPDXTagValueParser(SPDXTagValueBuilder(), SPDXWriterLogger())
        spdxparser.build()
        spdx_doc_orig, spdx_doc_orig_error = spdxparser.parse(file_content)
        spdx_doc_new, spdx_doc_new_error = spdxparser.parse(file_content)

        print(f"| Done. Generating SPDX file for Alien package...")
        spdx_doc_new.package.files = []
        for filename in apkg.internal_archive_checksums:
            matchingfile = None
            for debfile in spdx_doc_orig.package.files:
                if debfile.chk_sum.value == apkg.internal_archive_checksums[filename]:
                    debfile.name = filename
                    matchingfile = debfile
                    break

            if not matchingfile:
                matchingfile = self.__create_spdxfile(
                    filename,
                    apkg.internal_archive_checksums[filename],
                    utils.NoAssert(),
                    utils.NoAssert()
                )

            spdx_doc_new.package.files.append(matchingfile)

        for pkg_file in apkg.package_files:
            if '.tar' in pkg_file['name']:
                continue
            spdx_file = self.__create_spdxfile(
                pkg_file['name'],
                pkg_file['checksum'],
                pkg_file['license'] or utils.NoAssert(),
                utils.SPDXNone()  # FIXME: Should we add this to the alienmatcher.yaml?
            )
            spdx_doc_new.package.files.append(spdx_file)

        with open(spdx_new_filepath, 'w') as f:
            write_document(spdx_doc_new, f, validate=False)

        print(f"| Testing updated SPDX file: (no output means no problems)")
        spdx_doc_new.package.calc_verif_code()
        print(f"| Done. Final SPDX file result at {spdx_new_filepath}.")
        print(f"+-- SUCCESS.")

        return self.get(spdx_new_filepath)


def main():
    print(f"##########################################################################")
    print(f"### ALIENMATCHER v{VERSION}")
    print(f"##########################################################################")
    package_path = os.path.join(
        os.getcwd(),
        "solda-alienmatcher",
        "example",
        "alienpackage-zlib-1.2.11.tar.gz"
    )
    package = AlienPackage(package_path)
    pool_path = os.path.join(
        os.getcwd(),
        "tmp",
        "pool"
    )
    matcher = AlienMatcher(pool_path)
    matcher.match(package)

if __name__ == '__main__':
    main()

