from debian.debian_support import Version as DebVersion
from packaging.version import Version as PkgVersion, parse, InvalidVersion

class Version:

    FLAG_DEB_VERSION_ORIG = 1<<0
    FLAG_DEB_VERSION_SIMPLIFIED = 1<<1
    FLAG_DEB_VERSION_ERROR = 1<<2
    FLAG_PKG_VERSION_ORIG = 1<<3
    FLAG_PKG_VERSION_SIMPLIFIED = 1<<4
    FLAG_PKG_VERSION_ERROR = 1<<5

    MAX_DISTANCE = 10000000

    def __init__(self, version_str, remove_epoc=True):
        super().__init__()
        self.str = version_str
        if remove_epoc:
            self.str = self.__remove_epoc(self.str)
        self.version_conversion = 0
        self.str_simple = self.__version_simplify()
        self.package_version = None
        self.debian_version = None
        self.__make_debian_version()
        self.__make_package_version()

    def __make_package_version(self):
        try:
            self.package_version = PkgVersion(self.str)
            self.version_conversion |= Version.FLAG_PKG_VERSION_ORIG
        except Exception:
            try:
                self.package_version = PkgVersion(self.str_simple)
                self.version_conversion |= Version.FLAG_PKG_VERSION_SIMPLIFIED
            except Exception:
                self.version_conversion |= Version.FLAG_PKG_VERSION_ERROR

    def __make_debian_version(self):
        try:
            self.debian_version = DebVersion(self.str)
            self.version_conversion |= Version.FLAG_DEB_VERSION_ORIG
        except Exception:
            try:
                self.debian_version = DebVersion(self.str_simple)
                self.version_conversion |= Version.FLAG_DEB_VERSION_SIMPLIFIED
            except Exception:
                self.version_conversion |= Version.FLAG_DEB_VERSION_ERROR

    @staticmethod
    def __remove_epoc(vers_str):
        for i in range(len(vers_str)):
            if not vers_str[i].isdigit():
                break
        if i > 0 and i + 1 < len(vers_str) and vers_str[i] == ':':
            return vers_str[i+1:]
        return vers_str


    def __version_simplify(self):
        result = self.__remove_epoc(self.str)
        countdots = 0
        for i, ch in enumerate(result):
            if ch.isalpha() or ch in "+-~":
                return result[:i]
            if ch == ".":
                countdots += 1
                if countdots > 2:
                    return result[:i]

        return result

    def has_flag(self, flag):
        return (self.version_conversion & flag) != 0

    def __str__(self):
        simple = ""
        if (self.has_flag(Version.FLAG_DEB_VERSION_SIMPLIFIED | Version.FLAG_PKG_VERSION_SIMPLIFIED)):
            simple = " -> " + self.str_simple
        return f"{self.str}{simple} ({self.version_conversion:06b})"

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def __safe_abs_dist__(a, b):
        if not a:
            a = 0
        if not b:
            b = 0
        return abs(a - b)

    def distance(self, other_version):

        if not isinstance(other_version, Version):
            return Version.MAX_DISTANCE

        if self.has_flag(Version.FLAG_DEB_VERSION_ERROR | Version.FLAG_PKG_VERSION_ERROR):
            return Version.MAX_DISTANCE

        if other_version.has_flag(Version.FLAG_DEB_VERSION_ERROR | Version.FLAG_PKG_VERSION_ERROR):
            return Version.MAX_DISTANCE

        # https://packaging.pypa.io/en/latest/version.html
        dist_major = Version.__safe_abs_dist__(self.package_version.major, other_version.package_version.major)
        dist_minor = Version.__safe_abs_dist__(self.package_version.minor, other_version.package_version.minor)
        dist_micro = Version.__safe_abs_dist__(self.package_version.micro, other_version.package_version.micro)
        dist_post = Version.__safe_abs_dist__(self.package_version.post, other_version.package_version.post)

        dist_revision1 = 0
        if self.has_flag(Version.FLAG_DEB_VERSION_SIMPLIFIED | Version.FLAG_PKG_VERSION_SIMPLIFIED):
            dist_revision1 = 1

        dist_revision2 = 0
        if other_version.has_flag(Version.FLAG_DEB_VERSION_SIMPLIFIED | Version.FLAG_PKG_VERSION_SIMPLIFIED):
            dist_revision2 = 1

        # Defensive hierarchical distance, that is, consider subsequent versioning levels
        # only if the level above is equal.
        return (
            dist_major * 10000 +
            (dist_minor * 1000 if dist_major == 0 else 0) +
            (dist_micro * 100 if dist_major == 0 and dist_minor == 0 else 0) +
            ((dist_revision1 + dist_revision2 + dist_post) * 10 if dist_major == 0 and dist_minor == 0 and dist_micro == 0 else 0)
        )

    def distance_note(self, distance):
        if not isinstance(distance, int):
            return distance

        if distance == 0:
            return "GREEN"
        #if distance < 1000:
        #    return "GREEN"
        if distance < 10000:
            return "YELLOW"
        if distance < Version.MAX_DISTANCE:
            return "RED"
        return "VERSION INVALID"

    def __lt__(self, other):
        return self.debian_version < other.debian_version

    def __eq__(self, other):
        return self.str == other.str


if __name__ == '__main__':
    # dist = version_distance("2.31+git0+6fdf971c9d", "2.31-9")
    # print(dist)

    #v1 = Version("0.1.10-1")
    #v2 = Version("0.1.10")
    #v1 = Version("1:1.2.8.dfsg-2")

    #print(v1.distance(v2))

    #v1 = Version("8+deb8u11")
    v1 = Version("246")
    print(v1)
