# AlienMatcher

Tool to collect license and copyright information for a unknown source package,
which gets matched to a similar debian package. First, we try to find a similar
debian package, that is with the same name and a close version number. We use
semantic versioning schemes, and debian-specific versioning specificiations, to
calculate similarities between versions. Second, we match each file inside the
debian package to the given source package, and generate a SPDX file with
license information for each file. Files that are not present in the debian
package or differ from it, get a NOASSERTION tag.

Example usage:

```python
>>> from alienmatcher import AlienMatcher
>>> matcher = AlienMatcher("/whatever/path/to/cache/pool")  # Pool to cache downloads and API calls
>>> example = f"./example/alienpackage-zlib-1.2.11.tar.gz"
>>> package = AlienPackage(example)                         # See format specs below
>>> result_spdx = y2amatcher.match(y2apkg)                  # Find a matching package on Debian, and
                                                            #   build a SPDX file from it
```

## Requirements

- Python 3.6+
- https://git.ostc-eu.org/playground/debian2spdx
- https://pypi.org/project/python-debian/
- https://pypi.org/project/spdx-tools/

## AlienPackage specification

An `AlienPackage` consists of a `tar.gz` archive with the following content:
- A folder called `files`, which contains all files that should be checked
- The root folder contains a single file, named `alienmatcher.yaml`

Example of a `alienmatcher.yaml` file:
```yaml
version: 1                                        # mandatory; version of this yaml spec
package:                                          # mandatory; this package's content
  name: zlib                                      # mandatory; source package name
  version: 1.2.11                                 # mandatory; source package version
  comment: |                                      # optional; comment, about the package
    This is a comment about this package
    ...also multiline
  files:                                          # mandatory; description of the "files" folder content
    - name: ldflags-tests.patch                   # mandatory; full path to a file inside the "files" folder
      checksum: f370a10d1a45d16d65b32b6d2a9...    # mandatory; SHA1 checksum of the file (usually 40 chars)
      license: Zlib                               # optional; SPDX license identifier, if known
      provenance: file://ldflags-tests.patch      # optional; currently not used; where does this file come from?
    - name: run-ptest
      checksum: 8236e92debcc7a83144d0c4a3b5...
      license: CC0-1
      provenance: file://run-ptest
    - name: zlib-1.2.11.tar.xz
      checksum: e1cb0d5c92da8e9a8c2635dfa24...
      provenance: https://downloads.sourceforge.net/libpng/zlib/1.2.11/zlib-1.2.11.tar.xz
```

## Installation
**TODO**

## Command Line Interface
**TODO**

## Testing
**TODO**

## How it works
**TODO**
